const express = require('express');
const { Pool } = require('pg');

const app = express();
const PORT = process.env.PORT || 3000;

const pool = new Pool({
  user: process.env.PGUSER || 'myuser',
  host: process.env.PGHOST || 'postgres-service', // Use the service name for PostgreSQL
  database: process.env.PGDATABASE || 'mydatabase',
  password: process.env.PGPASSWORD || 'mypassword',
  port: process.env.PGPORT || 5432,
});

app.get('/', async (req, res) => {
  try {
    const result = await pool.query('SELECT $1::text as message', ['Hello, World! New']);
    const message = result.rows[0].message;

    res.send(message);
  } catch (error) {
    console.error('Error executing PostgreSQL query:', error);
    res.status(500).send('Internal Server Error');
  }
});

// Add some logging to help diagnose the issue
pool.on('error', (err) => {
  console.error('Unexpected error on idle PostgreSQL client', err);
  process.exit(-1); // Exit the application on a PostgreSQL error
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT} test`);
  console.log(`PostgreSQL Host: ${process.env.PGHOST || 'postgres-service'}`);
  console.log(`PostgreSQL Port: ${process.env.PGPORT || 5432}`);
});
